# Convolutional Neural Network C++

This is a Convolutional Neural Network (CNN) written in C++ using OpenCV 3.4. 

This is a code under construction that performs a forward propagation in the net and shows the result of convolutional operations made at every step, for illustrative or academic purposes.

## TO DO:

Code the Fully-Connected forward propagation steps

Code The Backpropagation Algorithm and CNN train functions.

## How to compile

In order to get this code to work, you'll need OpenCV 3.4 version or above, and a g++ compiler configured to link a project with the OpenCV library. Then, you only need to use the Makefile:

```
make
```

### Notes on OpenCV version 4

From the version 4, it's needed to update the main content of the Makefile to this:

```
g++ -Wall -o e cnn.cpp `pkg-config --cflags --libs opencv4` -lm -std=c++11
```
