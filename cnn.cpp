/*
	Red neuronal de convolución, con la siguiente arquitectura:
	- Capa de Entrada (IMG 32x32 GRAYSCALE)
	- Capa de Convolución (8 kernels de 3x3) -> IMG de características de 26x26x8
	- Capa de Max Pooling -> IMG de características de 13x13x8
	- Flattening -> IMG de características de 1352x1
	- Capa de salida, función Softmax

		Alex A. Turriza Suárez
*/

#include <opencv2/opencv.hpp>
#include <iostream>
#include <random>
#include <string>

//#define PATH "MNIST Dataset JPG format/MNIST Dataset JPG format/MNIST - JPG - testing/"

using namespace cv;
using namespace std;

void printMultidimMatrix(Mat * mtx)
{
	if(mtx->rows != -1 && mtx->cols != -1)
	{
		for(int i = 0; i < mtx->rows; i++)
		{
			for(int j = 0; j < mtx->cols; j++)
			{
				cout << (int)mtx->at<uchar>(i,j) << " ";
			}
			cout << endl;
		}
		cout << endl << endl;
	}
	else
	{
		for(int i = 0; i < mtx->size[2]; i++)
		{
			cout << "Capa " << i+1 << endl;
			for(int j = 0; j < mtx->size[0]; j++)
			{
				for(int k = 0; k < mtx->size[1]; k++)
				{
					cout << (int)mtx->at<uchar>(j,k,i) << " ";
				}
				cout << endl;
			}
			cout << endl << endl;
		}
	}
}

void printMtx(double * k, int cols, int rows)
{
	for(int i = 0; i < rows; i++)
	{
		for(int j = 0; j < cols; j++)
		{
			cout << k[i * cols + j] << " ";
		}
		cout << endl;
	}
	cout << endl;
}

void kernelInitializer(double * k, int kernelSize, int numKernels)
{
	default_random_engine generator;
	normal_distribution<double> ns; //Sin argumentos en el constructor, genera una normal estándar
	for(int i = 0; i < numKernels; i++)
	{
		for(int j = 0; j < kernelSize; j++)
		{
			for(int q = 0; q < kernelSize; q++)
			{
				k[i*(numKernels+1) + j*kernelSize + q] = ns(generator) / 10.0; //Xavier's Initialization;
			}
		}
	}
}

double getConvRes(Mat * img, double * k, int kSize, int x, int y)
{
	double suma = 0.0;
	int center = kSize / 2;
	int imgx = x + 1 - center;
	for(int i = kSize - 1; i >= 0; i--, imgx++)
	{
		int imgy = y + 1 - center;
		for(int j = kSize - 1; j >= 0; j--, imgy++)
		{	
			suma += k[i * kSize + j] * (double)img->at<uchar>(imgx, imgy);
		}
	}
	if(suma > 255)
	{
		return 255;
	}
	else if(suma < 0)
	{
		return 0;
	}
	return suma;
}

void Convolution(Mat * img, double * k, int kSize, int numKernels, Mat * res)
{
	double resp;
	cout << "Convolution function: " << endl;
	cout << "Dimensions: " << res->size[0] << ", " << res->size[1] << ", " << res->size[2] << endl;
	for(int i = 0; i < res->size[2]; i++)
	{
		for(int j = 0; j < res->size[0]; j++)
		{
			for(int q = 0; q < res->size[1]; q++)
			{
				resp = getConvRes(img, k + i*kSize*kSize, kSize, j, q);
				res->at<uchar>(j, q, i) = resp;
			}
		}
	}
}

void maxPooling(Mat * ch, Mat * res)
{
	int max = -1;
	uint8_t newVal;
	for(int i = 0; i < res->size[2]; i++)
	{
		for(int j = 0; j < res->size[0]; j++)
		{
			for(int k = 0; k < res->size[1]; k++)
			{			
				max = -1;
				for(int i_original = 2*j; i_original < 2*j + 2; i_original++)
				{
					for(int j_original = 2*k; j_original < 2*k + 2; j_original++)
					{
//						cout << "Max Pooling; Index: " << i_original << ", " << j_original << endl;
						newVal = ch->at<uchar>(i_original, j_original,i);
						if( max < newVal )
						{
							max = newVal;
						}
					}
				}
				res->at<uchar>(j,k,i) = max;
			}
		}
	}
}

void flattening(Mat * mxp, Mat * f)
{
	for(int i = 0; i < mxp->size[2]; i++)
	{
		for(int j = 0; j < mxp->size[0]; j++)
		{
			for(int k = 0; k < mxp->size[1]; k++)
			{
				f->at<uchar>(i*mxp->size[0]*mxp->size[0] + j*mxp->size[1] + k) = mxp->at<uchar>(j,k,i);
			}
		}
	}
}

void forwardStep(Mat * frame, double * kernel, int numKernels, Mat * c, Mat *maxp, Mat * f, int kernelSize)
{
	Convolution(frame, kernel, kernelSize, numKernels, c);
	maxPooling(c, maxp);
	flattening(maxp, f);
	cout << "Original image:" << endl;
	cout << *frame << endl << endl;
	cout << "Capas de convolucion" << endl;
	printMultidimMatrix(c);
	cout << "Capas de max pooling" << endl;
	printMultidimMatrix(maxp);
	cout << "Flattened" << endl;
	printMultidimMatrix(f);
}

int main()
{
	/*Declaración de variables*/
	int kernelSize = 3;
	int numKernels = 8;
	//string directory = PATH;
	//Mat actualFrame = imread("MNIST Dataset JPG format/MNIST Dataset JPG format/MNIST - JPG - testing/0/3.jpg", IMREAD_GRAYSCALE );
	Mat actualFrame = imread("MNIST Dataset JPG format/MNIST Dataset JPG format/MNIST - JPG - testing/0/3.jpg", IMREAD_GRAYSCALE );
	if(actualFrame.empty())
	{
		cout << "Error loading image" << endl;
		return -1;
	}
	double * kernel = NULL;
	int afterConv[] = {26, 26, 8}; //Dimensiones de la matriz después de la primera convolución
	int afterMaxPooling[] = {13, 13, 8}; //Dimensiones de la matriz contenedora del MAX POOLING
	int flattenDims[] = {1352,1,1}; //Dimensiones del elemento Flattened.
	Mat caract(3, afterConv, CV_8UC1); //Creación del contenedor de matrices caract.
	Mat maxPooled(3, afterMaxPooling, CV_8UC1); //Creación del contenedor de matrices caract.
	Mat flattened(1, flattenDims, CV_8UC1); //Creación del contenedor de matrices caract.
	kernel = new (nothrow) double[kernelSize * kernelSize * numKernels];
	if(kernel == NULL)
	{
		cout << "Error while assigning memory from heap" << endl;
		return -1;
	}

	/*Inicialización de elementos dinámicos*/
	kernelInitializer(kernel, kernelSize, numKernels);

	/*Pasos de la convolución*/
	forwardStep(&actualFrame, kernel, 8, &caract, &maxPooled, &flattened, kernelSize);
	return 0;
}



